/*
 * cc1101.c
 *
 *  Created on: Apr 9, 2021
 *      Author: Ilia Zarka
 */
#include "cc1101.h"

/**
 * Initializes all necessary register of the transceiver.
 */
void cc1101Init(struct cc1101 *cc1101){

	cc1101->setPin(cc1101->CS,1);

	commandStrobe(cc1101, SRES);

	//SMARTRF CONFIGURATIONS BEGIN

	//
	// Rf settings for CC1101
	//

	writeRegister(cc1101,0,0x01);	  //IOCFG2     GDO2 Output Pin Configuration
	writeRegister(cc1101,2,0x06);     //IOCFG0     GDO0 Output Pin Configuration
	writeRegister(cc1101,3,0x40);     //FIFOTHR    RX FIFO and TX FIFO Thresholds
	writeRegister(cc1101,8,0x05);     //PKTCTRL0   Packet Automation Control
	writeRegister(cc1101,11,0x06);    //FSCTRL1    Frequency Synthesizer Control
	writeRegister(cc1101,13,0x10);    //FREQ2      Frequency Control Word, High Byte
	writeRegister(cc1101,14,0xA7);    //FREQ1      Frequency Control Word, Middle Byte
	writeRegister(cc1101,15,0x62);    //FREQ0      Frequency Control Word, Low Byte
	writeRegister(cc1101,16,0xF5);    //MDMCFG4    Modem Configuration
	writeRegister(cc1101,17,0x83);    //MDMCFG3    Modem Configuration
	writeRegister(cc1101,18,0x03);    //MDMCFG2    Modem Configuration
	writeRegister(cc1101,19,0x00);    //MDMCFG1    Modem Configuration
	writeRegister(cc1101,21,0x15);    //DEVIATN    Modem Deviation Setting
	writeRegister(cc1101,24,0x18);    //MCSM0      Main Radio Control State Machine Configuration
	writeRegister(cc1101,25,0x16);    //FOCCFG     Frequency Offset Compensation Configuration
	writeRegister(cc1101,32,0xFB);    //WORCTRL    Wake On Radio Control
	writeRegister(cc1101,35,0xE9);    //FSCAL3     Frequency Synthesizer Calibration
	writeRegister(cc1101,36,0x2A);    //FSCAL2     Frequency Synthesizer Calibration
	writeRegister(cc1101,37,0x00);    //FSCAL1     Frequency Synthesizer Calibration
	writeRegister(cc1101,38,0x1F);    //FSCAL0     Frequency Synthesizer Calibration
	writeRegister(cc1101,44,0x81);    //TEST2      Various Test Settings
	writeRegister(cc1101,45,0x35);    //TEST1      Various Test Settings
	writeRegister(cc1101,46,0x09);    //TEST0      Various Test Settings


	//SMARTRF CONFIGURATIONS END

	//flush FIFOs
	commandStrobe(cc1101, SFRX);
	commandStrobe(cc1101, SFTX);

	//************************** to be tested ***********************

	//power Settings
	//Using PA settings from 0x61 to 0x6F is not allowed

	cc1101->TXPower.powerTable[0]=0x12;
	cc1101->TXPower.powerTable[1]=0x0E;
	cc1101->TXPower.powerTable[2]=0x1D;
	cc1101->TXPower.powerTable[3]=0x34;
	cc1101->TXPower.powerTable[4]=0x60;
	cc1101->TXPower.powerTable[5]=0x84;
	cc1101->TXPower.powerTable[6]=0xC8;
	cc1101->TXPower.powerTable[7]=0xC0;
	cc1101->TXPower.setting=4;

	changeOutputPower(cc1101, 0);


	//State MAchine Configuration
	writeRegister(cc1101, 0x17, 0b00111111);	//MCSM1		enter RX mode after sending/receiving

}

/**
 * Writes the desired value in the given address.
 */
void writeRegister(struct cc1101 *cc1101,uint8_t address, uint8_t value){
	address &= ~0x80;	//set write bit

	uint8_t tx[2]={address,value};
	uint8_t rx[2];

	cc1101->setPin(cc1101->CS,0);

	//wait until the CC1101 SO pin is LOW
	while(cc1101->readPin(cc1101->MISO)){
	}

	cc1101->spiSendReceive(cc1101->spi,tx,rx,2);

	cc1101->setPin(cc1101->CS,1);

	setStatusVariables(cc1101,rx[0]);

}

/**
 * Returns the value from the given register.
 */
uint8_t readRegister(struct cc1101 *cc1101,uint8_t address){

	address |= 0x80;	//set read bit

	uint8_t tx[2]={address,0};
	uint8_t rx[2];

	cc1101->setPin(cc1101->CS,0);

	//wait until the CC1101 SO pin is LOW
	while(cc1101->readPin(cc1101->MISO)){
	}

	cc1101->spiSendReceive(cc1101->spi,tx,rx,2);

	cc1101->setPin(cc1101->CS,1);

	setStatusVariables(cc1101,rx[0]);

	return rx[1];
}

/**
 * Writes the values given in the array to "size" number of consecutive registers,
 * starting from the address given.
 */
void burstWriteRegister(struct cc1101 *cc1101,uint8_t address, uint8_t *value, uint8_t size){

	address &= ~0x80;	//set write bit
	address |= 0x40;	//set burst bit


	uint8_t tx[1]={address};
	uint8_t rx[2];

	cc1101->setPin(cc1101->CS,0);

	//wait until the CC1101 SO pin is LOW
	while(cc1101->readPin(cc1101->MISO)){
	}

	cc1101->spiSendReceive(cc1101->spi,tx,rx,1);
	for(int i=0; i<size; i++){
		cc1101->spiSendReceive(cc1101->spi,&value[i],&rx[1],1);
	}

	cc1101->setPin(cc1101->CS,1);

	setStatusVariables(cc1101,rx[0]);

}

/**
 * Reads the values from to "size" number of consecutive registers,
 * starting from the address given. Data is stored in the "value" array.
 */
void burstReadRegister(struct cc1101 *cc1101,uint8_t address, uint8_t *value, uint8_t size){

	address |= 0x80;	//set read bit
	address |= 0x40;	//set burst bit

	uint8_t tx[1]={address};
	uint8_t rx[1];

	cc1101->setPin(cc1101->CS,0);

	//wait until the CC1101 SO pin is LOW
	while(cc1101->readPin(cc1101->MISO)){
	}

	cc1101->spiSendReceive(cc1101->spi,tx,rx,1);

	for(int i=0; i<size; i++){
		cc1101->spiSendReceive(cc1101->spi,tx,&value[i],1);
	}

	cc1101->setPin(cc1101->CS,1);

	setStatusVariables(cc1101,rx[0]);
}

/**
 * Increases/decreases the output power by "increment"(negative to decrease).
 * The plausible values are stored in an array in the cc1101 struct on initialization
 */
int changeOutputPower(struct cc1101 *cc1101,int increment){

	int newSetting=((int)cc1101->TXPower.setting) + increment;
	if(newSetting<0){
		writeRegister(cc1101, PATABLE, cc1101->TXPower.powerTable[cc1101->TXPower.setting]);
		cc1101->TXPower.setting=0;
		return 1;
	}
	if(newSetting>7){
		cc1101->TXPower.setting=7;
		writeRegister(cc1101, PATABLE, cc1101->TXPower.powerTable[cc1101->TXPower.setting]);
		return 1;
	}

	cc1101->TXPower.setting=(uint8_t)newSetting;

	writeRegister(cc1101, PATABLE, cc1101->TXPower.powerTable[cc1101->TXPower.setting]);

	return 0;
}

/**
 * Gets the header byte and extracts status information.
 */
void setStatusVariables(struct cc1101 *cc1101, uint8_t value){
	cc1101->status.statusRegValue = value;
	cc1101->status.FIFO_BYTES_AVAILABLE = value & 0B00001111;
	cc1101->status.STATE = (value & 0B01110000) >> 4;
	cc1101->status.CHIP_RDYn = (value & 0B10000000) >> 7;

}

/**
 * Sends command strobes.
 */
void commandStrobe(struct cc1101 *cc1101,uint8_t address){
	address &= ~0x40;
	address |= 0x80;
	uint8_t rx[1]={0};

	cc1101->setPin(cc1101->CS,0);

	//wait until the CC1101 SO pin is LOWs
	uint32_t start = cc1101->getTick();
	while(cc1101->readPin(cc1101->MISO)){
		if((cc1101->getTick() - start) > TIMEOUT){
			break;
		}
	}

	cc1101->spiSendReceive(cc1101->spi,&address,rx,1);

	cc1101->setPin(cc1101->CS,1);

	setStatusVariables(cc1101,rx[0]);
}

/**
 * Transmits the data.
 * Returns 0 if it succeeded.
 */
void sendBytes(struct cc1101 *cc1101, uint8_t *data, uint8_t size){

	//if anything but RX mode, go to idle and flush TX FIFO
	uint32_t start;
	if(cc1101->status.STATE!=STATE_IS_RX){
		commandStrobe(cc1101, SIDLE);
		start = cc1101->getTick();
		while(cc1101->status.STATE!=STATE_IS_IDLE){
			commandStrobe(cc1101, SNOP);
			if((cc1101->getTick() - start) > TIMEOUT){
				break;
			}
		}
		commandStrobe(cc1101, SFTX);
		start = cc1101->getTick();
		while(cc1101->status.STATE!=STATE_IS_IDLE){
			commandStrobe(cc1101, SNOP);
			if((cc1101->getTick() - start) > TIMEOUT){
				break;
			}
		}
	}

	//get the TX FIFO ready
	writeRegister(cc1101, FIFO_ADDRESS, size);
	for(int i=0; i<size; i++){
		writeRegister(cc1101, FIFO_ADDRESS, data[i]);
	}

	//issue the transmission
	commandStrobe(cc1101, STX);

    //wait until it starts sending
	start = cc1101->getTick();
	while(cc1101->status.STATE!=STATE_IS_TX){
		commandStrobe(cc1101, SNOP);
		if((cc1101->getTick() - start) > TIMEOUT){
			break;
		}
	}

	//wait until sending is complete
	start = cc1101->getTick();
	while(cc1101->status.STATE==STATE_IS_TX){
		commandStrobe(cc1101, SNOP);
		if((cc1101->getTick() - start) > TIMEOUT){
			break;
		}
	}

}

/**
 * Returns the number of bytes that the transmitter sent.
 */
uint8_t getNumOfRXBytes(struct cc1101 *cc1101){
	return readRegister(cc1101, RXBYTES_ADDRESS);
}

/**
 * Reads the message and stores it in the "data" array.
 * The size of the data (bufferSize) array must be greater
 * or equal to the packet length received.
 * Returns 0 if it succeeded.
 */
int getMessage(struct cc1101 *cc1101, uint8_t *data, uint16_t bufferSize, uint32_t timeout){

	//No message is received yet
	if(getNumOfRXBytes(cc1101) == 0){
		return 1;
	}

	uint8_t  length= readRegister(cc1101,FIFO_ADDRESS);

	//Wait for the whole message
	uint32_t start = cc1101->getTick();
	while(getNumOfRXBytes(cc1101) < length + 2){
		uint32_t time=cc1101->getTick();
		while(time==cc1101->getTick()){

		}
		//If time is out
		if((cc1101->getTick() - start) > timeout){
			//Flush RX FIFO
			commandStrobe(cc1101, SIDLE);
			commandStrobe(cc1101, SFRX);
			start = cc1101->getTick();
			while(calibrate(cc1101)){
				if((cc1101->getTick() - start) > TIMEOUT){
					break;
				}
			}
			return 2;
		}
	}

	//The message won't fit in the buffer
	if(length > bufferSize){

		//Flush RX FIFO
		commandStrobe(cc1101, SIDLE);
		commandStrobe(cc1101, SFRX);
		start = cc1101->getTick();
		while(calibrate(cc1101)){
			if((cc1101->getTick() - start) > TIMEOUT){
				break;
			}
		}
		return 3;
	}

	burstReadRegister(cc1101, FIFO_ADDRESS, data, length);

	//Read the last status bytes

	//convert rssi register value to dBm
	int rssi = (int)readRegister(cc1101,FIFO_ADDRESS);
	if(rssi>=128){
		cc1101->status.RSSI=((rssi - 256)/2) - RSSI_OFFSET;
	}else{
		cc1101->status.RSSI=(rssi/2) - RSSI_OFFSET;
	}

	//read LQI & CRC
	uint8_t LQI_CRC = readRegister(cc1101,FIFO_ADDRESS);

	cc1101->status.LQI= LQI_CRC & 0b01111111;

	cc1101->status.CRC_OK = LQI_CRC & 0b10000000;

	return 0;
}

/**
 * Returns 1 if the PLL is in lock
 */
int pllIsInLock(struct cc1101 *cc1101){

	//section 22.1 in the datasheet
	if(readRegister(cc1101, 0x25) == 0x3F){
		return 0;
	}
	return 1;
}

/**
 * Calibrates the transceiver
 */
uint8_t calibrate(struct cc1101 *cc1101){

	//issue idle command
	commandStrobe(cc1101, SIDLE);
	uint32_t start=cc1101->getTick();
	while(cc1101->status.STATE !=STATE_IS_IDLE){
		commandStrobe(cc1101, SNOP);
		//if it takes too long
		if((cc1101->getTick() - start) > TIMEOUT){
			return 1;
		}
	}

	//issue calibration command
	commandStrobe(cc1101, SCAL);
	start=cc1101->getTick();
	while(!pllIsInLock(cc1101)){
		//if it takes too long
		if((cc1101->getTick() - start) > TIMEOUT){
			return 1;
		}
	}

	//go back to RX
	commandStrobe(cc1101, SRX);
	start=cc1101->getTick();
	while(cc1101->status.STATE !=STATE_IS_RX){
		commandStrobe(cc1101, SNOP);
		//if it takes too long
		if((cc1101->getTick() - start) > TIMEOUT){
			return 1;
		}
	}
	return 0;
}
