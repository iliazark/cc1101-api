### CC1101 API

#### Description

This projects aim is to make it easy to use the CC1101 transiever with a wide range of controllers (arduinos, stm32s, etc.). Hardware call functions need to be implemented by the user, although most are as easy as calling the corresponding function on your development enviroment of choice.

#### Configurations

TIs SmartRF studio was used for configuring the tranceiver. The API was tested on 433 MHz trancievers so the configurations shown below will be for that band. Though everything should work for all the supported bands (if configured correctly).

![image1](/images/SmartRF_main.png)

On the sub-1 GHz tab, select the CC1101 tranceiver. This new window should pop up.

![image1](/images/SmartRF_DeviceControl_panel.png)

Here you need to set RF parameters such as modulation format or base frequency. For further information on what these are I strongly recomend reading the datasheet of the tranceiver. If you only need a set of parameters that just works, you can copy the configurations in the CC1101.c file in the example project.
Once you are done setting te RF parameters, you hit the register export button on te top right corner. The following window should pop up.

![image1](/images/SmartRF_Register_Export.png)

In the "Registers:" box type this string "writeRegister(cc1101,\@AD\@,0x\@VH\@);\@<<\@\@<<\@//\@RN\@\@<<\@\@Rd\@" as shown in the image above. Now click on "Copy to Clipboard" and paste it in the cc1101.c file, in the cc1101Init function between the SMARTRF CONFIGURATIONS BEGIN and END comments.

#### Coding

For the API to work there are some handlers and some functions that need to be set-mapped to whatever your platform uses. In the example I used an STM32 microcontroller so I used the HAL functions that the CubeIDE imporded during the configuration of the project.

The first thing that the user has to change is the pin and spi structs from the cc1101.h file. these structs should contain everything necessary to use distinguish and use all the pins and the spi interfaces of the controller. In my case, CubeIDE uses a port and a pin number for the IO pins and a TypeDef handler for the spi interfaces. ArduinoIDE for example uses just a number for the pins. All these should be between the BEGIN and END comments.

Then in the main code, the following functions should be implemented and mapped to the corresponding pointers in the cc1101 struct.

- uint8_t readPin(struct pin pin)
    - this function should return 0 if the pin given as an argument is low and 1 otherwise
- void setPin(struct pin pin,uint8_t value)
    - this function should set the given pin low if value == 0 and high otherwise
- void spiSendReceive(struct spi spi,uint8_t *tx, uint8_t *rx, uint16_t size)
    - this function should sen size number of bytes from the tx pointer through the spi given as an agument, while simultaneously reading the same spi and storing the read values in the rx pointer (most IDEs have existing functions for this. For example CubeIDE has the SPI_TransmitReceive from the HAL libraries and Arduino has the SPI.transfer function).
- uint32_t getTick();
    - this function should return the number of miliseconds from the boot time of the controller.

After these functions are implemented they should be mapped to the corresponding function pointers as shown in the main.c file in the example.

#### Project status

Not every functionality of the tranceiver is implemented in this API. I just published what i had to write for a personal project, although I will try and add everything I can in my free time.
