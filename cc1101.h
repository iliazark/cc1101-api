/*
 * cc1101.h
 *
 *  Created on: Apr 9, 2021
 *      Author: Ilia Zarka
 */

#ifndef INC_CC1101_H_
#define INC_CC1101_H_

#include "main.h"

/**
 * For a more detailed command description,
 * see the manufacturers datasheet
 */
#define SRES 0x30		//Reset chip
#define SFSTXON 0x31	//Enable and calibrate frequency synthesizer
#define SXOFF 0x32		//Turn off crystal oscillator
#define SCAL 0x33		//Calibrate frequency synthesizer and turn it off
#define SRX 0x34		//Enable RX
#define STX 0x35		//In IDLE state: Enable TX
#define SIDLE 0x36		//Exit RX / TX
#define SWOR 0x38		//Start automatic RX polling sequence
#define SPWD 0x39		//Enter power down mode when CSn goes high
#define SFRX 0x3A		//Flush the RX FIFO buffer
#define SFTX 0x3B		//Flush the TX FIFO buffer
#define SWORRST 0x3C	//Reset real time clock to Event1 value
#define SNOP 0x3D		//No operation

#define FIFO_ADDRESS 0x3F		//TX-RX FIFOs' address
#define TXBYTES_ADDRESS 0xFA	//Underflow and number of bytes in the TX FIFO
#define RXBYTES_ADDRESS 0xFB	//Overflow and number of bytes in the RX FIFO
#define PATABLE 0x3E			//PATABLE's address

#define STATE_IS_IDLE				0B000
#define STATE_IS_RX					0B001
#define STATE_IS_TX					0B010
#define STATE_IS_FSTXON				0B011
#define STATE_IS_CALIBRATE			0B100
#define STATE_IS_SETTLING			0B101
#define STATE_IS_RXFIFO_OVERFLOW	0B110
#define STATE_IS_TXFIFO_UNDERFLOW	0B111

#define RSSI_OFFSET 74	//as stated in the datasheet
#define TIMEOUT 1000	//timeout for commands in ms

/**
 * This struct should hold the necessary information
 * to control and read GPIO pins
 */
struct pin{
	//GPIO PIN BEGIN

	GPIO_TypeDef *gpio;
	uint16_t pin;

	//GPIO PIN END
};

/**
 * This struct should hold the necessary information
 * to use the spi interface
 */
struct spi{
	//SPI HANDLE BEGIN

	SPI_HandleTypeDef *spi;

	//SPI HANDLE END
};

struct status{
	uint8_t CHIP_RDYn;
	uint8_t STATE;
	uint8_t FIFO_BYTES_AVAILABLE;
	uint8_t statusRegValue;
	int RSSI;
	uint8_t LQI;
	uint8_t CRC_OK;
};

struct TXPower{
	uint8_t powerTable[8];
	uint8_t setting;
};

struct cc1101{
	struct pin CS;
	struct pin MISO;
	struct spi spi;
	struct status status;
	void (*setPin)(struct pin pin,uint8_t value);
	uint8_t (*readPin)(struct pin pin);
	void (*spiSendReceive)(struct spi spi,uint8_t *tx, uint8_t *rx, uint16_t size);
	uint32_t (*getTick)();
	struct TXPower TXPower;

};

void cc1101Init(struct cc1101 *cc1101);
void writeRegister(struct cc1101 *cc1101,uint8_t address, uint8_t value);
uint8_t readRegister(struct cc1101 *cc1101,uint8_t address);
void burstWriteRegister(struct cc1101 *cc1101,uint8_t address, uint8_t *value, uint8_t size);
void burstReadRegister(struct cc1101 *cc1101,uint8_t address, uint8_t *value, uint8_t size);
int changeOutputPower(struct cc1101 *cc1101,int increment);
void setStatusVariables(struct cc1101 *cc1101, uint8_t value);
void commandStrobe(struct cc1101 *cc1101,uint8_t address);
void sendBytes(struct cc1101 *cc1101, uint8_t *data, uint8_t size);
uint8_t getNumOfRXBytes(struct cc1101 *cc1101);
int getMessage(struct cc1101 *cc1101, uint8_t *data, uint16_t bufferSize, uint32_t timeout);
int pllIsInLock(struct cc1101 *cc1101);
uint8_t calibrate(struct cc1101 *cc1101);

#endif /* INC_CC1101_H_ */
